from django.urls import path

from . import views

urlpatterns = [
        # recebe inteiro como numero e passa a Fatorial em views.py
        path ("<int:numero>", views.Fatorial, name = "Fatorial"),
]
