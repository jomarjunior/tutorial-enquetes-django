from django.shortcuts import render

# Create your views here.
# Pagina para testes e brainstorming.

from django.http import HttpResponse

# Pagina que calcula fatorial
def CalcularFatorial ( numero ):
    if ( numero <= 1 ):
        return 1
    return numero * CalcularFatorial ( numero - 1 )

def Fatorial (request, numero):
    html = "<title>Fatorial de %s</title>" % numero
    html += "<body><div>%s</div></body>" % CalcularFatorial ( numero )
    return HttpResponse (html)
