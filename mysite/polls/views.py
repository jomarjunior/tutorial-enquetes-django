# Create your views here.
from django.utils import timezone
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from django.db.models import F

from .models import Choice, Question

class IndexView (generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset (self):
        """Retornar as ultimas 5 perguntas exceto as publicadas no futuro."""
        return Question.objects.filter (pub_date__lte = timezone.now()).order_by ('-pub_date')[:5]

class DetailView (generic.DetailView):
    model = Question
    template_name = 'polls/detail.html'

    def get_queryset (self):
        return Question.objects.filter (pub_date__lte = timezone.now())

class ResultsView (generic.DetailView):
    model = Question
    template_name = 'polls/results.html'

    def get_queryset (self):
        return Question.objects.filter (pub_date__lte = timezone.now())

def vote (request, question_id):
    # Obter a pergunta
    question = get_object_or_404 (Question, pk = question_id)
    
    # Tentar encontrar a resposta escolhida.
    try:
        # request.POST é um dicionario com os dados recebidos em POST.
        # Ele sempre vai retornar em STRING o valor fornecido em um metodo POST.
        selected_choice = question.choice_set.get (pk = request.POST['choice'])
    except ( KeyError, Choice.DoesNotExist ):
        # Retornar ao form de votacao caso nao encontre a resposta selecionada
        return render (request, 'polls/detail.html', {
            'question': question,
            'error_message': "You didn't select a choice."})
    # Conseguimos identificar a resposta selecionda.
    # Usar F() para realizar a operacao de incremento dentro do banco de dados.
    # Evita problemas quando temos multiplos usuarios votando ao mesmo tempo.
    selected_choice.votes = F('votes') + 1
    selected_choice.save()
    # A funcao F() se mantem depois de um save(). Assim, sempre que save() for chamado
    # o banco de dados tenderia a realizar novamente o incremento, para evitar isso
    # adicionamos um refresh no objeto.
    selected_choice.refresh_from_db()
    
    # Sempre retornar um HttpResponseRedirect em POSTs.
    return HttpResponseRedirect (reverse ('polls:results', args = (question.id,)))
