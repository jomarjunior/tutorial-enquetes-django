#imports
from django.urls import path

from . import views

app_name = 'polls'
#code body
urlpatterns = [
        # Pagina inicial, /polls/
        path ("", views.IndexView.as_view(), name = "index"),
        
        # Pagina de detalhes, /polls/5/.
        # Recebe o numero inteiro que aparecer apos /polls/ e passa a detail() em views.py como question_id.
        path ("<int:pk>/", views.DetailView.as_view(), name = "detail"),

        # Pagina que apresenta os resultados de determinada questao.
        # Recebe o numero inteiro que aparecer apos /polls/ e passa a results() em views.py como question_id.
        path ("<int:pk>/results/", views.ResultsView.as_view(), name = "results"),

        # Pagina para votacao em determinada questao.
        # Recebe o numero inteiro que aparecer apos /polls/ e passa a vote() em views.py como question_id.
        path ("<int:question_id>/vote/", views.vote, name = "vote"),
]
