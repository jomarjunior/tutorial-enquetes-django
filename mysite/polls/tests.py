import datetime

from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

from .models import Question

# Create your tests here

class QuestionModelTests (TestCase):
    def test_was_published_recently_with_future_question (self):
        """ was_published_recently() deve retornar Falso no caso de uma pergunta
        no futuro """
        time = timezone.now() + datetime.timedelta (days = 30)
        future_question = Question (pub_date = time)
        self.assertIs (future_question.was_published_recently(), False)

    def test_was_published_recently_with_recent_question (self):
        """ was_published_recently() deve retornar True no caso de uma pergunta
        dentro de um intervalo de um dia do tempo atual."""
        time = timezone.now() - datetime.timedelta (hours = 23, minutes = 59, seconds = 59)
        recent_question = Question (pub_date = time)
        self.assertIs (recent_question.was_published_recently(), True)

    def test_was_published_recently_with_old_question (self):
        """ was_published_recently() deve retornar False no caso de uma pergunta
        feita ha mais de 1 dia do tempo atual."""
        time = timezone.now() - datetime.timedelta (days = 1, seconds = 1)
        old_question = Question (pub_date = time)
        self.assertIs (old_question.was_published_recently(), False)

def create_question (question_text, days):
    """ Cria uma questao com um dado question_text e um offset days de dias a partir da data atual,
    tanto no passado (-) quanto no futuro (+). """
    time = timezone.now() + datetime.timedelta ( days = days )
    return Question.objects.create (question_text = question_text, pub_date = time)

class QuestionIndexViewTests (TestCase):
    def test_no_questions (self):
        """ Checa se nao ha perguntas. """
        response = self.client.get(reverse('polls:index'))
        self.assertEqual (response.status_code, 200)
        self.assertContains (response, "No polls are available.")
        self.assertQuerysetEqual (response.context ['latest_question_list'], [])

    def test_past_question (self):
        """ Perguntas com pub_date no passado sao exibidas na pagina index? """
        create_question (question_text = "Past question.", days = -30)
        response = self.client.get (reverse ('polls:index'))
        self.assertQuerysetEqual (
                response.context['latest_question_list'],
                ['<Question: Past question.>'] # 'Past question.' nesse caso tem q ser igual ao definido ao criar a pergunta.
                )

    def test_future_question (self):
        """ Perguntas com pub_date no futuro sao exibidas na pagina index? """
        create_question (question_text = "Future question.", days = 30)
        response = self.client.get (reverse('polls:index'))
        self.assertContains (response, "No polls are available.")
        self.assertQuerysetEqual (response.context['latest_question_list'], [])

    def test_future_question_and_past_question (self):
        """ No caso da existencia de questoes no futuro e no passado, mesmo assim apresentar
        apenas as questoes no passado. """
        create_question (question_text = "Past question.", days = -30)
        create_question (question_text = "Future question.", days = 30)
        response = self.client.get (reverse('polls:index'))
        self.assertQuerysetEqual (
                response.context['latest_question_list'],
                ['<Question: Past question.>'] # 'Past question.' nesse caso tem q ser igual ao definido ao criar a pergunta.
                )

    def test_two_past_questions (self):
        """ Multiplas questoes na pagina index. """
        create_question (question_text = "Past question 1.", days = -30)
        create_question (question_text = "Past question 2.", days = -5)
        response = self.client.get (reverse('polls:index'))
        self.assertQuerysetEqual (
                response.context['latest_question_list'],
                ['<Question: Past question 2.>', '<Question: Past question 1.>']
                )

class QuestionDetailViewTests (TestCase):
    def test_future_question (self):
        """ O detail view de uma pergunta no futuro deve retornar um erro 404. """
        future_question = create_question (question_text = 'Future question.', days = 5)
        url = reverse ('polls:detail', args = (future_question.id,))
        response = self.client.get(url)
        self.assertEqual (response.status_code, 404)

    def test_past_question (self):
        """ o detail view de uma pergunta no passado deve retornar o texto da pergunta. """
        past_question = create_question (question_text = 'Past question.', days = -5)
        url = reverse ('polls:detail', args = (past_question.id,))
        response = self.client.get(url)
        self.assertContains (response, past_question.question_text)

class QuestionResultsViewTests (TestCase):
    def test_future_question (self):
        """ O results view de uma pergunta no futuro deve retornar um erro 404. """
        future_question = create_question (question_text = 'Future question.', days = 5)
        url = reverse ('polls:results', args = (future_question.id,))
        response = self.client.get(url)
        self.assertEqual (response.status_code, 404)
        
    def test_past_question (self):
        """ o results view de uma pergunta no passado deve retornar algo. """
        past_question = create_question (question_text = 'Past question.', days = -5)
        url = reverse ('polls:results', args = (past_question.id,))
        response = self.client.get(url)
        self.assertEqual (response.status_code, 200)
